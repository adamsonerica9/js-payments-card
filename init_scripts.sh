#!/usr/bin/env bash
set -eu -o pipefail -E

curl -s "https://gitlab.com/api/v4/projects/26971281/packages/generic/init_cicd_scripts/latest/init_scripts.sh" | /bin/bash -s -- "$@"
