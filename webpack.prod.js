const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

const plugins = [
  new webpack.DefinePlugin({
    WEBSERVICES_URL: JSON.stringify('https://library.securetrading.net:8443'),
  }),
];

module.exports = merge(common, {
  mode: 'production',
  plugins,
});
