## Submission Guidelines

## Code Style, Naming convention

> - [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
> - [Gitflow](https://pl.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
> - DRY, SOLID, KISS, YAGNI
> - name of branch - feature/ST-number-of-task-name-of-task - for more information please refer to the [Gitflow](https://pl.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
