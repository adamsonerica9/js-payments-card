const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

const plugins = [
  new CleanWebpackPlugin(),
  new WebpackManifestPlugin(),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: './example/index.html',
    chunks: ['example'],
  }),
  new MiniCssExtractPlugin({
    filename: '[name].css',
    chunkFilename: '[id].css',
  }),
  new StyleLintPlugin({
    context: path.join(__dirname, 'src'),
  }),
  new webpack.DefinePlugin({
    HOST: JSON.stringify(process.env.npm_package_config_host),
  }),
  new webpack.ProvidePlugin({
    process: 'process/browser',
    Buffer: ['buffer', 'Buffer'],
  }),
  new CopyPlugin({
    patterns: [{
      from: 'example/images',
      to: '[name][ext]',
      force: true,
    }],
  }),
];

module.exports = {
  entry: {
    example: './example/index.ts',
    index: [
      './src/shared/imports/polyfills.ts',
      './src/index.ts',
    ],
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist'),
    publicPath: '',
    library: 'Card',
    libraryExport: 'default',
    libraryTarget: 'umd',
  },
  plugins,
  optimization: {
    minimizer: [new TerserPlugin({ extractComments: false })],
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)$/,
        use: ['babel-loader', 'source-map-loader'],
        include: [
          path.join(__dirname, 'src'),
          path.join(__dirname, 'test'),
          path.join(__dirname, 'example'),
        ],
      },
      {
        test: /\.ts$/,
        enforce: 'pre',
        use: [
          {
            loader: 'source-map-loader',
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        exclude: [/index\.html$/, /node_modules/],
        use: { loader: 'html-loader' },
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
          },
        ],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
};
