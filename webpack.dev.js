const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const webpack = require('webpack');

const plugins = [
  new webpack.DefinePlugin({
    WEBSERVICES_URL: JSON.stringify('.'),
  }),
];

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  plugins,
  devServer: {
    https: true,
    host: '0.0.0.0',
    port: 8443,
    static: {
      directory: path.join(__dirname, './dist'),
    },
    client: false,
  },
});
