import { CardWrapper } from './components/card-wrapper/CardWrapper';
import { IConfig } from './components/card-wrapper/CardWrapper.interface';

export default (config: IConfig): CardWrapper => new CardWrapper(config);
