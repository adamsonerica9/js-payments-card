// @ts-ignore
import template from '../card/Card.html';
import '../card/Card.scss';
import {
  CARD_NUMBER_PROPERTIES,
  EXPIRATION_DATE_PROPERTIES,
  SECURITY_CODE_PROPERTIES,
} from '../card/CardProperties.const';
import { CARD_SELECTORS } from '../card/CardSelectors.const';
import { Card } from '../card/Card.component';
import { Utils } from '../../services/utils/Utils.service';
import { Validator } from '../../services/validator/Validator.service';
import { IConfig, IErrorContainers, IFields, IInputContainers } from './CardWrapper.interface';

export class CardWrapper {
  private static FOUR_DIGITS_CARDS: string[] = ['AMEX'];
  private static MATCH_EXACTLY_FOUR_DIGITS = '^[0-9]{4}$';
  private static MATCH_EXACTLY_THREE_DIGITS = '^[0-9]{3}$';
  private static SECURITY_CODE_PLACEHOLDER = 'XXX';
  private static SECURITY_CODE_EXTENDED_PLACEHOLDER = 'XXXX';
  private animatedCardTargetContainer: HTMLDivElement;
  private card: Card;
  private cardNumberError: HTMLElement;
  private cardNumberInput: HTMLInputElement;
  private expirationDateError: HTMLElement;
  private expirationDateInput: HTMLInputElement;
  private securityCodeError: HTMLElement;
  private securityCodeInput: HTMLInputElement;
  private validation: Validator;

  constructor(config: IConfig) {
    const { animatedCardContainer, locale, fields } = config;
    this.init(fields, animatedCardContainer, locale, config);
  }

  onCardNumberChange(value: string, outsideValue: boolean): void {
    this.card.onCardNumberChanged(value, outsideValue);
  }

  onExpirationDateChange(value: string, outsideValue: boolean): void {
    this.card.onExpirationDateChanged(value, outsideValue);
  }

  onSecurityCodeChange(value: string, outsideValue: boolean): void {
    this.card.onSecurityCodeChanged(value, outsideValue);
  }

  onFieldFocusOrBlur(focused: boolean): void {
    this.card.onFieldFocusOrBlur(focused);
  }

  onCardNumberInput(id: string, callback: (event: Event) => void): void {
    this.cardNumberInput.addEventListener('blur', () => {
      this.cardNumberInput.blur();
      this.validation.luhnCheck(this.cardNumberInput);
      this.validation.validate(this.cardNumberInput, this.cardNumberError);
    });

    this.cardNumberInput.addEventListener('focus', () => {
      this.cardNumberInput.focus();
    });

    this.cardNumberInput.addEventListener('input', (event: KeyboardEvent) => {
      callback(event);
      const { nonformat } = this.card.onCardNumberChanged(this.cardNumberInput.value);
      this.changeSecurityCodePattern(nonformat);
      if (this.isActive(this.cardNumberInput)) {
        this.validation.keepCursorAtSamePosition(this.cardNumberInput);
      }
    });

    this.cardNumberInput.addEventListener('keydown', (event: KeyboardEvent) => {
      this.validation.setKeyDownProperties(this.cardNumberInput, event);
    });

    this.cardNumberInput.addEventListener('paste', (event: ClipboardEvent) => {
      const value = this.validation.onPaste(event);
      // @ts-ignore
      this.card.onCardNumberChanged(value);
      this.changeSecurityCodePattern(this.cardNumberInput.value);
      if (this.isActive(this.cardNumberInput)) {
        this.validation.keepCursorAtSamePosition(this.cardNumberInput);
      }
    });
  }

  onExpirationDateInput(id: string, callback: (event: Event) => void): void {
    this.expirationDateInput.addEventListener('blur', () => {
      this.expirationDateInput.blur();
      this.validation.validate(this.expirationDateInput, this.expirationDateError);
    });

    this.expirationDateInput.addEventListener('focus', () => {
      this.expirationDateInput.focus();
    });

    this.expirationDateInput.addEventListener('input', event => {
      callback(event);
      this.card.onExpirationDateChanged(this.expirationDateInput.value);
      if (this.isActive(this.expirationDateInput)) {
        this.validation.keepCursorAtSamePosition(this.expirationDateInput);
      }
    });

    this.expirationDateInput.addEventListener('keydown', (event: KeyboardEvent) => {
      this.validation.setKeyDownProperties(this.expirationDateInput, event);
    });
  }

  onSecurityCodeInput(id: string, callback: (event: Event) => void): void {
    this.securityCodeInput.addEventListener('blur', () => {
      this.securityCodeInput.blur();
      this.validation.validate(this.securityCodeInput, this.securityCodeError);
      this.card.flipCard();
    });

    this.securityCodeInput.addEventListener('focus', () => {
      this.securityCodeInput.focus();
      this.card.flipCard();
    });

    this.securityCodeInput.addEventListener('input', (event: KeyboardEvent) => {
      callback(event);
      this.card.onSecurityCodeChanged(this.securityCodeInput.value);
    });
  }

  private addInputs(inputs: IInputContainers): void {
    this.cardNumberInput = document.getElementById(inputs.cardNumber) as HTMLInputElement;
    this.expirationDateInput = document.getElementById(inputs.expirationDate) as HTMLInputElement;
    this.securityCodeInput = document.getElementById(inputs.securityCode) as HTMLInputElement;
    this.setInputsAttributes(CARD_NUMBER_PROPERTIES, this.cardNumberInput);
    this.setInputsAttributes(EXPIRATION_DATE_PROPERTIES, this.expirationDateInput);
    this.setInputsAttributes(SECURITY_CODE_PROPERTIES, this.securityCodeInput);
  }

  private addInputErrorLabels(errors: IErrorContainers): void {
    this.cardNumberError = document.getElementById(errors.cardNumber) as HTMLInputElement;
    this.expirationDateError = document.getElementById(errors.expirationDate) as HTMLInputElement;
    this.securityCodeError = document.getElementById(errors.securityCode) as HTMLInputElement;
  }

  private addAnimatedCardContainer(animatedCardContainer: string): void {
    this.animatedCardTargetContainer = document.getElementById(animatedCardContainer) as HTMLDivElement;
    if (this.animatedCardTargetContainer) {
      this.animatedCardTargetContainer.innerHTML = template;
    }
  }

  private changeSecurityCodePattern(value: string): void {
    if (CardWrapper.FOUR_DIGITS_CARDS.includes(this.card.getCardDetails(value).type)) {
      this.securityCodeInput.setAttribute('pattern', CardWrapper.MATCH_EXACTLY_FOUR_DIGITS);
      this.securityCodeInput.setAttribute('placeholder', CardWrapper.SECURITY_CODE_EXTENDED_PLACEHOLDER);
    } else {
      this.securityCodeInput.setAttribute('pattern', CardWrapper.MATCH_EXACTLY_THREE_DIGITS);
      this.securityCodeInput.setAttribute('placeholder', CardWrapper.SECURITY_CODE_PLACEHOLDER);
    }
  }

  private init(fields: IFields, animatedCardContainer: string, locale: string, config: IConfig): void {
    this.validation = new Validator(locale);
    this.setInputsAndErrorsTargets(fields);
    this.addAnimatedCardContainer(animatedCardContainer);
    this.setCardConfig(config);
  }

  private setCardConfig(config: IConfig): void {
    if (Utils.ifElementExists(CARD_SELECTORS.ANIMATED_CARD_INPUT_SELECTOR)) {
      this.card = new Card(config);
    } else {
      throw new Error('Animated card input does not exist');
    }
  }

  private setInputsAndErrorsTargets(fields: { inputs: IInputContainers; errors: IErrorContainers }): void {
    if (fields) {
      if (fields.inputs && fields.errors) {
        this.addInputs(fields.inputs);
        this.addInputErrorLabels(fields.errors);
      }
    }
  }

  private setInputsAttributes(attributes: Record<string, string>, element: HTMLInputElement): void {
    Object.keys(attributes).map((item: string) => {
      element.setAttribute(item, attributes[item]);
    });
  }

  private isActive(element: HTMLElement): boolean {
    return element === document.activeElement;
  }
}

