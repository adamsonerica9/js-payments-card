import { iinLookup } from '@trustpayments/ts-iin-lookup';
import { BrandDetailsType } from '@trustpayments/ts-iin-lookup/dist/types';
import { DomMethods } from '../../services/dom-methods/DomMethods.service';
import { Formatter } from '../../services/formatter/Formatter.service';
import { Utils } from '../../services/utils/Utils.service';
import { IConfig, IFields } from '../card-wrapper/CardWrapper.interface';
import { Translator } from '../../services/translator/Translator.service';
import { CARD_DETAILS_PLACEHOLDERS, CARD_TYPES } from './CardType.const';
import { CARD_CLASSES, CARD_COMPONENT_CLASS, CARD_SELECTORS } from './CardSelectors.const';
import { cardsLogos } from './CardLogos.const';
import { ICardDetails } from './Card.interface';

export class Card extends Utils {
  private static DEFAULT_LANGUAGE = 'en_GB';
  private static DISABLED_ATTRIBUTE = 'disabled';
  private static NOT_FLIPPED_CARDS: string[] = [CARD_TYPES.AMEX];

  private static enableInput(inputId: string): void {
    document.getElementById(inputId).removeAttribute(Card.DISABLED_ATTRIBUTE);
  }

  private animatedCardContainer: HTMLInputElement;
  private cardDetails: ICardDetails = {
    cardNumber: CARD_DETAILS_PLACEHOLDERS.CARD_NUMBER,
    expirationDate: CARD_DETAILS_PLACEHOLDERS.EXPIRATION_DATE,
    flippable: true,
    logo: '',
    securityCode: CARD_DETAILS_PLACEHOLDERS.SECURITY_CODE,
    type: CARD_DETAILS_PLACEHOLDERS.TYPE,
  };
  private cardNumberId: string;
  private expirationDateId: string;
  private securityCodeId: string;
  private securityCodeMessageId: string;
  private formatter: Formatter;
  private locale: string;

  constructor(config: IConfig) {
    super();
    const { fields } = config;
    this.init(fields, config);
  }

  onCardNumberChanged(cardNumber: string, outsideValue?: boolean): { nonformat: string } {
    const { type, nonformat } = this.setCardNumberDetails(cardNumber);
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_CREDIT_CARD_ID, this.cardDetails.cardNumber);
    type ? this.setTheme() : this.resetTheme();
    this.setSecurityCode(outsideValue);
    return { nonformat };
  }

  onExpirationDateChanged(expirationDate: string, outsideValue?: boolean): void {
    this.cardDetails.expirationDate = this.formatter.date(
      this.getContent(expirationDate, CARD_DETAILS_PLACEHOLDERS.EXPIRATION_DATE),
      this.expirationDateId,
      outsideValue
    );
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_EXPIRATION_DATE_ID, this.cardDetails.expirationDate);
  }

  onSecurityCodeChanged(securityCode: string, outsideValue?: boolean): void {
    this.cardDetails.securityCode = this.formatter.code(securityCode, this.securityCodeId, outsideValue);
    this.setSecurityCode(outsideValue);
  }

  onFieldFocusOrBlur(focused: boolean): void {
    if (this.isFlippableCard(this.cardDetails.type) && this.animatedCardContainer) {
      if (focused) {
        this.animatedCardContainer.classList.add(CARD_CLASSES.CLASS_FOR_ANIMATION);
      } else {
        this.animatedCardContainer.classList.remove(CARD_CLASSES.CLASS_FOR_ANIMATION);
      }
    }
  }

  flipCard(): void {
    if (this.isFlippableCard(this.cardDetails.type)) {
      this.animatedCardContainer.classList.contains(CARD_CLASSES.CLASS_FOR_ANIMATION)
        ? this.animatedCardContainer.classList.remove(CARD_CLASSES.CLASS_FOR_ANIMATION)
        : this.animatedCardContainer.classList.add(CARD_CLASSES.CLASS_FOR_ANIMATION);
    } else {
      this.animatedCardContainer.classList.remove(CARD_CLASSES.CLASS_FOR_ANIMATION);
    }
  }

  getCardDetails = (value: string): BrandDetailsType => iinLookup.lookup(value);
  private isAmex = (content: string): boolean => content === CARD_TYPES.AMEX;
  private isFlippableCard = (type: string): boolean => !Card.NOT_FLIPPED_CARDS.includes(type);
  private returnThemeClass = (theme: string): string => `${CARD_COMPONENT_CLASS}__${theme}`;

  private init(fields: IFields, config: IConfig): void {
    this.locale = config.locale ? config.locale : Card.DEFAULT_LANGUAGE;
    this.translator = new Translator(this.locale);
    this.formatter = new Formatter(this.locale);
    this.setInputsAndErrors(fields);
    this.setCardContent();
    this.animatedCardContainer = this.getElement(CARD_SELECTORS.ANIMATED_CARD_INPUT_SELECTOR);
  }

  private setInputsAndErrors(fields: IFields): void {
    if (fields) {
      if (fields.inputs && fields.errors) {
        this.cardNumberId = fields.inputs.cardNumber;
        this.expirationDateId = fields.inputs.expirationDate;
        this.securityCodeId = fields.inputs.securityCode;
        this.securityCodeMessageId = fields.errors.securityCode;
      }
    }
  }

  private setCardContent(): void {
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_CREDIT_CARD_LABEL, Translator.translations.LABEL_CARD_NUMBER);
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_EXPIRATION_DATE_LABEL, Translator.translations.LABEL_EXPIRATION_DATE);
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_LABEL, Translator.translations.LABEL_SECURITY_CODE);
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_CREDIT_CARD_ID, this.cardDetails.cardNumber);
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_EXPIRATION_DATE_ID, Translator.translations.PLACEHOLDER_EXPIRATION_DATE);
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_BACK_ID, this.cardDetails.securityCode);
  }

  private setCardNumberDetails(cardNumber: string): { type: string; nonformat: string } | null {
    const { value, nonformat } = this.formatter.number(
      this.getContent(cardNumber, CARD_DETAILS_PLACEHOLDERS.CARD_NUMBER),
      this.cardNumberId
    );
    const type: string = this.toLower(this.getCardDetails(nonformat).type);
    this.cardDetails.cardNumber = value;
    this.cardDetails.type = type;
    this.cardDetails.flippable = this.isFlippableCard(type);
    return { type, nonformat };
  }

  private addSecurityCodeOnBack(): void {
    DomMethods.addClass(
      this.getElement(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_FRONT_ID),
      CARD_CLASSES.CLASS_SECURITY_CODE_HIDDEN
    );
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_BACK_ID, this.cardDetails.securityCode);
  }

  private addSecurityCodeOnFront(): void {
    DomMethods.removeClass(
      this.getElement(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_FRONT_ID),
      CARD_CLASSES.CLASS_SECURITY_CODE_HIDDEN
    );
    this.setContent(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_FRONT_FIELD_ID, this.cardDetails.securityCode);
  }

  private setSecurityCode(outsideValue: boolean): void {
    if (!outsideValue) {
      Card.enableInput(this.securityCodeId);
    }
    if (!this.isAmex(this.cardDetails.type)) {
      this.setSecurityCodePlaceholder(CARD_DETAILS_PLACEHOLDERS.SECURITY_CODE);
      this.addSecurityCodeOnBack();

      return;
    }

    this.setSecurityCodePlaceholder(CARD_DETAILS_PLACEHOLDERS.SECURITY_CODE_EXTENDED);
    this.addSecurityCodeOnFront();

    return;
  }

  private setSecurityCodePlaceholder(placeholder: string): void {
    if (
      this.cardDetails.securityCode === '' ||
      this.cardDetails.securityCode === CARD_DETAILS_PLACEHOLDERS.SECURITY_CODE ||
      this.cardDetails.securityCode === CARD_DETAILS_PLACEHOLDERS.SECURITY_CODE_EXTENDED
    ) {
      this.cardDetails.securityCode = placeholder;
    }
  }

  private toggleLogoClasses(toRemove: string, toAdd: string): void {
    DomMethods.removeClass(this.getElement(CARD_CLASSES.CLASS_LOGO_WRAPPER), `${toRemove}`);
    DomMethods.addClass(this.getElement(CARD_CLASSES.CLASS_LOGO_WRAPPER), `${toAdd}`);
  }

  private clearThemeClasses(): void {
    this.setAttr(
      CARD_SELECTORS.ANIMATED_CARD_SIDE_FRONT,
      'class',
      `${CARD_CLASSES.CLASS_SIDE} ${CARD_CLASSES.CLASS_FRONT}`
    );
    this.setAttr(
      CARD_SELECTORS.ANIMATED_CARD_SIDE_BACK,
      'class',
      `${CARD_CLASSES.CLASS_SIDE} ${CARD_CLASSES.CLASS_BACK}`
    );
  }

  private resetTheme(): void {
    this.toggleLogoClasses(CARD_CLASSES.CLASS_LOGO, CARD_CLASSES.CLASS_LOGO_DEFAULT);
    this.clearThemeClasses();
    this.clearContent(CARD_SELECTORS.ANIMATED_CARD_SECURITY_CODE_BACK_ID);
    this.removeLogo();
  }

  private setTheme(): void {
    this.setThemeClasses();
    this.getLogoURI(this.cardDetails.type);
    this.addLogo();
  }

  private setThemeClasses(): void {
    this.toggleLogoClasses(CARD_CLASSES.CLASS_LOGO_DEFAULT, CARD_CLASSES.CLASS_LOGO);
    this.clearThemeClasses();
    DomMethods.addClass(
      this.getElement(CARD_SELECTORS.ANIMATED_CARD_SIDE_FRONT),
      this.returnThemeClass(this.cardDetails.type)
    );
    DomMethods.addClass(
      this.getElement(CARD_SELECTORS.ANIMATED_CARD_SIDE_BACK),
      this.returnThemeClass(this.cardDetails.type)
    );
  }

  private addLogo(): string {
    const { logo, type } = this.cardDetails;
    const ifImageExists = !!document.getElementById(CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID);
    if (ifImageExists && !!logo) {
      DomMethods.setProperty.apply(this, ['src', logo, CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID]);
      DomMethods.setProperty.apply(this, ['alt', type, CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID]);
    } else if (!ifImageExists && !!logo) {
      this.setLogo(logo, type);
    } else {
      return logo;
    }
  }

  private createLogo(logo: string, type: string): HTMLElement {
    return DomMethods.createHtmlElement.apply(this, [
      {
        alt: type,
        class: CARD_CLASSES.CLASS_LOGO_IMAGE,
        id: CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID,
        src: logo,
      },
      'img',
    ]);
  }

  private getLogoURI(type: string): string {
    this.cardDetails.logo = cardsLogos[type];
    return cardsLogos[type];
  }

  private removeLogo(): void {
    if (document.getElementById(CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID)) {
      DomMethods.removeChildFromDOM.apply(this, [
        CARD_CLASSES.CLASS_LOGO_WRAPPER,
        CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID,
      ]);
    }
  }

  private setLogo(logo: string, type: string): void {
    DomMethods.appendChildIntoDOM(CARD_CLASSES.CLASS_LOGO_WRAPPER, this.createLogo(logo, type));
    DomMethods.setProperty.apply(this, ['src', logo, CARD_SELECTORS.ANIMATED_CARD_PAYMENT_LOGO_ID]);
  }
}
