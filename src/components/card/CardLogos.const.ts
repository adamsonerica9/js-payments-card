// @ts-ignore
import amex from './logos/Amex.png';
// @ts-ignore
import astropaycard from './logos/Astropay.png';
// @ts-ignore
import diners from './logos/Diners.png';
// @ts-ignore
import discover from './logos/Discover.png';
// @ts-ignore
import jcb from './logos/Jcb.png';
// @ts-ignore
import maestro from './logos/Maestro.png';
// @ts-ignore
import mastercard from './logos/Mastercard.png';
// @ts-ignore
import visa from './logos/Visa.png';

export const cardsLogos: { [brandName: string]: string } = {
  amex,
  astropaycard,
  default: '',
  diners,
  discover,
  jcb,
  maestro,
  mastercard,
  visa,
};
