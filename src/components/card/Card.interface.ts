interface ICardDetails {
  cardNumber: string;
  expirationDate: string;
  flippable: boolean;
  logo: string;
  securityCode: string;
  type: string;
}

export { ICardDetails };
