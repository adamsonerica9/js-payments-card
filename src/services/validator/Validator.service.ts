import { iinLookup } from '@trustpayments/ts-iin-lookup';
import { BrandDetailsType } from '@trustpayments/ts-iin-lookup/dist/types';
import { luhnCheck } from '@trustpayments/ts-luhn-check';
import { Utils } from '../utils/Utils.service';
import { Translator } from '../translator/Translator.service';

export class Validator {
  protected static STANDARD_FORMAT_PATTERN = '(\\d{1,4})(\\d{1,4})?(\\d{1,4})?(\\d+)?';
  private static BACKSPACE_KEY_CODE = 8;
  private static CARD_NUMBER_DEFAULT_LENGTH = 16;
  private static DELETE_KEY_CODE = 46;
  private static MATCH_CHARS = /[^\d]/g;
  private static MATCH_DIGITS = /^[0-9]*$/;
  private static SECURITY_CODE_DEFAULT_LENGTH = 3;
  private static ERROR_CLASS = 'error';
  private static CURSOR_SINGLE_SKIP = 1;
  private static CURSOR_DOUBLE_SKIP = 2;

  protected cardNumberValue: string;
  protected expirationDateValue: string;
  protected securityCodeValue: string;
  private translator: Translator;
  private currentKeyCode: number;
  private selectionRangeEnd: number;
  private selectionRangeStart: number;
  private matchDigitsRegexp: RegExp;
  private cursorSkip = 0;

  constructor(locale: string) {
    this.translator = new Translator(locale);
    this.matchDigitsRegexp = new RegExp(Validator.MATCH_DIGITS);
  }

  setKeyDownProperties(element: HTMLInputElement, event: KeyboardEvent): void {
    this.currentKeyCode = event.keyCode;
    this.selectionRangeStart = element.selectionStart;
    this.selectionRangeEnd = element.selectionEnd;
  }

  keepCursorAtSamePosition(element: HTMLInputElement): void {
    const lengthFormatted: number = element.value.length;
    const isLastCharSlash: boolean = element.value.charAt(lengthFormatted - 2) === '/';
    const start: number = this.selectionRangeStart;
    const end: number = this.selectionRangeEnd;

    if (this.isPressedKeyDelete()) {
      element.setSelectionRange(start, end);
    } else if (this.isPressedKeyBackspace()) {
      element.setSelectionRange(start - Validator.CURSOR_SINGLE_SKIP, end - Validator.CURSOR_SINGLE_SKIP);
    } else if (isLastCharSlash) {
      ++this.cursorSkip;
      element.setSelectionRange(start + Validator.CURSOR_DOUBLE_SKIP, end + Validator.CURSOR_DOUBLE_SKIP);
    } else if (element.value.charAt(end) === ' ') {
      ++this.cursorSkip;
      element.setSelectionRange(start + Validator.CURSOR_DOUBLE_SKIP, end + Validator.CURSOR_DOUBLE_SKIP);
    } else {
      element.setSelectionRange(start + Validator.CURSOR_SINGLE_SKIP, end + Validator.CURSOR_SINGLE_SKIP);
    }
  }

  luhnCheck(element: HTMLInputElement): void {
    const { value } = element;
    luhnCheck(value) ? element.setCustomValidity('') : element.setCustomValidity('luhn');
  }

  validate(element: HTMLInputElement, errorContainer: HTMLElement): void {
    const { customError, patternMismatch, valid, valueMissing } = element.validity;
    if (!valid) {
      if (valueMissing) {
        this.setError(element, errorContainer, 'Field is required');
      } else if (patternMismatch) {
        this.setError(element, errorContainer, 'Value mismatch pattern');
      } else if (customError) {
        this.setError(element, errorContainer, 'Value mismatch pattern');
      } else {
        this.setError(element, errorContainer, 'Invalid field');
      }
    } else {
      this.removeError(element, errorContainer);
    }
  }

  onPaste(event: ClipboardEvent): DataTransfer | string {
    event.preventDefault();
    let { clipboardData } = event;
    if (typeof clipboardData === 'undefined') {
      // @ts-ignore
      clipboardData = window.clipboardData.getData('Text');
    } else {
      // @ts-ignore
      clipboardData = event.clipboardData.getData('text/plain');
    }
    return clipboardData;
  }

  protected cardNumber(value: string): void {
    this.cardNumberValue = this.removeNonDigits(value);
    const cardDetails = this.getCardDetails(this.cardNumberValue);
    const length = cardDetails.type
      ? Utils.getLastElementOfArray(cardDetails.length)
      : Validator.CARD_NUMBER_DEFAULT_LENGTH;
    this.cardNumberValue = this.limitLength(this.cardNumberValue, length);
  }

  protected expirationDate(value: string): void {
    this.expirationDateValue = this.removeNonDigits(value);
  }

  protected securityCode(value: string): void {
    this.securityCodeValue = this.removeNonDigits(value);
    const cardDetails = this.getCardDetails(this.cardNumberValue);
    const cardLength = Utils.getLastElementOfArray(cardDetails.cvcLength);
    const length = cardDetails.type && cardLength ? cardLength : Validator.SECURITY_CODE_DEFAULT_LENGTH;
    this.securityCodeValue = this.limitLength(this.securityCodeValue, length);
  }

  protected getCardDetails(cardNumber = ''): BrandDetailsType {
    return iinLookup.lookup(cardNumber);
  }

  protected isPressedKeyBackspace(): boolean {
    return this.currentKeyCode === Validator.BACKSPACE_KEY_CODE;
  }

  protected isPressedKeyDelete(): boolean {
    return this.currentKeyCode === Validator.DELETE_KEY_CODE;
  }

  protected limitLength(value: string, length: number): string {
    return value.substring(0, length);
  }

  protected removeNonDigits(value: string): string {
    return value.replace(Validator.MATCH_CHARS, '');
  }

  private setError(element: HTMLInputElement, errorContainer: HTMLElement, errorMessage: string): void {
    element.classList.add(Validator.ERROR_CLASS);
    errorContainer.textContent = this.translator.translate(errorMessage);
  }

  private removeError(element: HTMLInputElement, errorContainer: HTMLElement): void {
    element.classList.remove(Validator.ERROR_CLASS);
    errorContainer.textContent = '';
  }
}
