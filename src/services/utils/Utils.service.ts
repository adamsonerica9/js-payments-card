import { Translator } from '../translator/Translator.service';

export class Utils {

  static ifElementExists(id: string): HTMLInputElement {
    return document.getElementById(id) as HTMLInputElement;
  }

  static inArray<T>(array: ArrayLike<T>, item: T): boolean {
    return Array.from(array).indexOf(item) >= 0;
  }

  static getLastElementOfArray = (array: number[]): number => array && array.slice(-1).pop();

  static setElementAttributes(attributes: Record<string, string>, element: HTMLInputElement): void {
    // tslint:disable-next-line: forin
    for (const attribute in attributes) {
      const value = attributes[attribute];
      if (Utils.inArray(['value'], attribute)) {
        // @ts-ignore
        element[attribute] = value;
      } else if (!value) {
        element.removeAttribute(attribute);
      } else {
        element.setAttribute(attribute, value);
      }
    }
  }

  static stripChars(string: string, regex = /[\D+]/g): string {
    return string.replace(regex, '');
  }

  static forEachBreak<inputType, returnType>(
    iterable: ArrayLike<inputType>,
    callback: (item: inputType) => returnType
  ): returnType {
    let result: returnType = null;
    // tslint:disable-next-line:forin
    for (const i in iterable) {
      result = callback(iterable[i]);
      if (result) {
        break;
      }
    }
    return result || null;
  }

  protected translator: Translator;

  constructor() {
    this.translator = new Translator('en_GB');
  }

  protected getElement = (id: string): HTMLInputElement => document.getElementById(id) as HTMLInputElement;
  protected getContent = (value: string, placeholder: string): string => (value ? value : placeholder);
  protected setContent = (id: string, text: string): string  =>
    (this.getElement(id).textContent = this.translator.translate(text));
  protected setAttr = (id: string, attr: string, value: string): void  => this.getElement(id).setAttribute(attr, value);
  protected toLower = (content: string | null): string  => (content ? content.toLowerCase() : content);
  protected clearContent = (id: string): string  => this.setContent(id, '');
}
