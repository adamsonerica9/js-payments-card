/**
 * @jest-environment jsdom
 */

import { CARD_SELECTORS } from '../../components/card/CardSelectors.const';
import { Utils } from './Utils.service';

describe('Utils', () => {
  const { instance } = UtilsFixture();

  describe('ifCardWrapperExist', () => {

    it('should return card wrapper element', () => {
      expect(Utils.ifElementExists(CARD_SELECTORS.ANIMATED_CARD_INPUT_SELECTOR).getAttribute('id')).toEqual(
        CARD_SELECTORS.ANIMATED_CARD_INPUT_SELECTOR
      );
    });
  });

  describe('inArray', () => {
    const arrayLike: number[] = [1, 2, 3, 4];

    it('should given element exists in array like object', () => {
      expect(Utils.inArray(arrayLike, 3)).toBe(true);
    });

    it('should given element doesnt exist in array like object', () => {
      expect(Utils.inArray(arrayLike, 5)).toBe(false);
    });
  });

  describe('getLastElementOfArray', () => {
    const array: number[] = [1, 2, 3, 4];
    const arrayEmpty: number[] = [];

    it('should return last element if array exists', () => {
      expect(Utils.getLastElementOfArray(array)).toEqual(4);
    });

    it('should return undefined if array is not defined', () => {
      expect(Utils.getLastElementOfArray(undefined)).toBe(undefined);
    });

    it('should return undefined if array is empty', () => {
      expect(Utils.getLastElementOfArray(arrayEmpty)).toBe(undefined);
    });
  });

  describe('setElementAttributes', () => {
    const element: HTMLInputElement = document.createElement('input');

    it('should return value (as card value only attribute)', () => {
      element.setAttribute('value', '');
      // @ts-ignore
      Utils.setElementAttributes(
        {
          value: '1111 1111',
        },
        element
      );
      // @ts-ignore
      expect(element.getAttribute('value')).toEqual('');
    });

    it('should set proper attributes given in params', () => {

      Utils.setElementAttributes(
        {
          firstAttribute: 'FUUUUUUUUUUUUUU',
          secondAttribute: 'Like a sir',
          thirdAttribute: 'Pepe the Frog',
        },
        element
      );
      // @ts-ignore
      expect(element.getAttribute('firstAttribute')).toEqual('FUUUUUUUUUUUUUU');
      // @ts-ignore
      expect(element.getAttribute('secondAttribute')).toEqual('Like a sir');
      // @ts-ignore
      expect(element.getAttribute('thirdAttribute')).toEqual('Pepe the Frog');
    });

    it('should clear value attribute when its specified as false', () => {
      Utils.setElementAttributes(
        {
          // @ts-ignore
          thirdAttribute: false,
        },
        element
      );
      // @ts-ignore
      expect(element.getAttribute('thirdAttribute')).toEqual(null);
    });
  });

  describe('stripChars', () => {

    it('should get rid of the chars from string', () => {
      expect(Utils.stripChars('123abc')).toEqual('123');
    });

    it('should get rid of all digits from string', () => {
      expect(Utils.stripChars('123abc', /[0-9]/g)).toEqual('abc');
    });
  });

  describe('forEachBreak', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    it.each<any>([
      [[], null, 0],
      [[0, 0, 0, 0], null, 4], // if we don't get a truthy result we iterate the whole length
      [[0, 0, 4, 6], 4, 3], // short circuits after the first truthy result
      [[null, null, 4, 6], 4, 3], // this is more like what we do in _lookup
      [{ 0: 0, 1: 0, 2: 4, 3: 6 }, 4, 3], // behaves like return Object.values(iterable).some(callback)
    ])('should return desired value and call the callback', (iterable, expected, timesCalledBack) => {
      const callback = jest.fn(
        (item: unknown): unknown => {
          return item;
        }
      );
      expect(Utils.forEachBreak(iterable, callback)).toBe(expected);
      expect(callback).toHaveBeenCalledTimes(timesCalledBack);
    });
  });

  describe('getContent', () => {

    it('should return value if it is specified', () => {
      // @ts-ignore
      expect(instance.getContent('test value', 'placeholder')).toEqual('test value');
    });

    it('should return placeholder if value is not specified', () => {
      // @ts-ignore
      expect(instance.getContent('', 'placeholder')).toEqual('placeholder');
    });
  });

  describe('setAttr', () => {
    const { testElementId } = UtilsFixture();

    it('should set given attribute to given value', () => {
      // @ts-ignore
      instance.setAttr(testElementId, 'someAttr', 'someValue');
      expect(document.getElementById(testElementId).getAttribute('someAttr')).toEqual('someValue');
    });
  });

  describe('toLower', () => {

    it('should return to lower case if text is specified', () => {
      // @ts-ignore
      expect(instance.toLower('SoME CONTent')).toEqual('some content');
    });

    it('should return null if text is not specified', () => {
      // @ts-ignore
      expect(instance.toLower(null)).toEqual(null);
      // @ts-ignore
      expect(instance.toLower(undefined)).toEqual(undefined);
      // @ts-ignore
      expect(instance.toLower('')).toEqual('');
    });
  });

  describe('clearContent', () => {
    const { instance, testElementId, testText } = UtilsFixture();

    it('should clear content in given element', () => {
      // @ts-ignore
      instance.translator.translate = jest.fn().mockReturnValueOnce('some translation');
      // @ts-ignore
      instance.setContent(testElementId, testText);
      // @ts-ignore
      instance.clearContent(testElementId);
      expect(document.getElementById(testElementId).textContent).toEqual('');
    });
  });
});

function UtilsFixture() {
  const html =
    '<form id="st-form" class="example-form"> <h1 class="example-form__title" id="test-title"> Trust Payments<span>AMOUNT: <strong>10.00 GBP</strong></span> </h1> <div class="example-form__section example-form__section--horizontal"> <div class="example-form__group"> <label for="example-form-name" class="example-form__label example-form__label--required">NAME</label> <input id="example-form-name" class="example-form__input" type="text" placeholder="John Doe" autocomplete="name" /> </div> <div class="example-form__group"> <label for="example-form-email" class="example-form__label example-form__label--required">E-MAIL</label> <input id="example-form-email" class="example-form__input" type="email" placeholder="test@mail.com" autocomplete="email" /> </div> <div class="example-form__group"> <label for="example-form-phone" class="example-form__label example-form__label--required">PHONE</label> <input id="example-form-phone" class="example-form__input" type="tel" placeholder="+00 000 000 000" autocomplete="tel" /> </div> </div> <div class="example-form__spacer"></div> <div class="example-form__section"  id="example-frame"> <div id="st-notification-frame" class="example-form__group"></div> <div id="st-card-number" class="example-form__group"></div> <div id="st-expiration-date" class="example-form__group"></div> <div id="st-security-code" class="example-form__group"></div> <div id="st-error-container" class="example-form__group"></div> <div class="example-form__spacer"></div> </div> <div class="example-form__section"> <div class="example-form__group"> <button type="submit" class="example-form__button">PAY</button> </div> </div> <div class="example-form__section"> <div id="st-control-frame" class="example-form__group"></div> <div id="st-visa-checkout" class="example-form__group"></div> <div id="st-apple-pay" class="example-form__group"></div> </div> <div id="st-animated-card" class="st-animated-card-wrapper"></div> </form>';
  document.body.innerHTML = html;
  const testElementId = 'test-title';
  const testText  =
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A at consectetur cupiditate dicta doloremque dolores eum facilis minima, minus optio pariatur possimus quaerat quas quod rem sunt velit voluptatibus voluptatum?';
  const instance = new Utils();
  return { instance, testText, testElementId };
}
