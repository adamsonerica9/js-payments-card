export class DomMethods {

  static addClass = (element: HTMLElement, classToAdd: string): void => element.classList.add(classToAdd);

  static appendChildIntoDOM(target: string, child: HTMLElement): HTMLElement {
    const element = document.getElementById(target)
      ? document.getElementById(target)
      : document.getElementsByTagName('body')[0];
    element.appendChild(child);
    return element;
  }

  static createHtmlElement = (attributes: Record<string, string>, markup: string): HTMLElement => {
    const element = document.createElement(markup);

    Object.keys(attributes).map(item => element.setAttribute(item, attributes[item]));
    return element;
  };

  static removeClass = (element: HTMLElement, classToRemove: string): void => element.classList.remove(classToRemove);

  static removeChildFromDOM(parentId: string, childId: string): HTMLElement {
    const parent = document.getElementById(parentId);
    const child = document.getElementById(childId);
    if (parent && child) {
      parent.removeChild(child);
    } else {
      throw new Error('Parent or child element is not specified ');
    }
    return parent;
  }

  static setProperty(attr: string, value: string, elementId: string): HTMLElement {
    const element = document.getElementById(elementId);
    element.setAttribute(attr, value);
    return element;
  }
}
