import { CARD_DETAILS_PLACEHOLDERS } from '../../components/card/CardType.const';
import { Utils } from '../utils/Utils.service';
import { Validator } from '../validator/Validator.service';

export class Formatter extends Validator {
  private blocks: number[] = [2, 2];
  private cardNumberFormatted: string;
  private dateBlocks = {
    currentDateMonth: '',
    currentDateYear: '',
    previousDateYear: '',
  };

  constructor(locale: string) {
    super(locale);
  }

  number(cardNumber: string, id: string): { value: string, nonformat: string } {
    super.cardNumber(cardNumber);
    let selectEnd;
    let selectStart;
    const element: HTMLInputElement = document.getElementById(id) as HTMLInputElement;
    const cardNumberCleaned: string = this.removeNonDigits(this.cardNumberValue);
    if (element) {
      element.value = cardNumberCleaned;
      selectEnd = element.selectionEnd;
      selectStart = element.selectionStart;
    }
    const cardDetails = this.getCardDetails(cardNumberCleaned);
    const format = cardDetails ? cardDetails.format : Formatter.STANDARD_FORMAT_PATTERN;
    const previousValue = cardNumberCleaned;
    let value = previousValue;
    if (format && value.length > 0) {
      value = Utils.stripChars(value, undefined);
      let matches = value.match(new RegExp(format, '')).slice(1);
      if (Utils.inArray(matches, undefined)) {
        matches = matches.slice(0, matches.indexOf(undefined));
      }
      const matched = matches.length;
      if (format && matched > 1) {
        const preMatched = previousValue.split(' ').length;
        selectStart += matched - preMatched;
        selectEnd += matched - preMatched;
        value = matches.join(' ');
      }
    }

    if (value !== previousValue && element) {
      Utils.setElementAttributes({ value }, element);
      element.setSelectionRange(selectStart, selectEnd);
    }
    this.cardNumberFormatted = value;
    this.cardNumberValue = value.replace(/\s/g, '');
    value = value ? value : CARD_DETAILS_PLACEHOLDERS.CARD_NUMBER;
    return { value, nonformat: this.cardNumberValue };
  }

  date(value: string, id?: string, outsideValue?: boolean): string {
    super.expirationDate(value);
    let result = '';

    this.blocks.forEach(length => {
      if (this.expirationDateValue.length > 0) {
        const sub = this.expirationDateValue.slice(0, length);
        const rest = this.expirationDateValue.slice(length);
        result += sub;
        this.expirationDateValue = rest;
      }
    });
    let fixedDate = this.dateFixed(result);
    if (!outsideValue) {
      const element: HTMLInputElement = document.getElementById(id) as HTMLInputElement;
      element.value = fixedDate;
    }
    fixedDate = fixedDate ? fixedDate : CARD_DETAILS_PLACEHOLDERS.EXPIRATION_DATE;
    return fixedDate;
  }

  code(value: string, id?: string, outsideValue?: boolean): string {
    super.securityCode(value);
    if (!outsideValue) {
      const element: HTMLInputElement = document.getElementById(id) as HTMLInputElement;
      element.value = this.securityCodeValue;
    }
    return this.securityCodeValue;
  }

  private dateISO(previousDate: string[], currentDate: string[]): string {
    this.dateBlocks.currentDateMonth = currentDate[0];
    this.dateBlocks.currentDateYear = currentDate[1];
    this.dateBlocks.previousDateYear = previousDate[1];

    if (!this.dateBlocks.currentDateMonth.length) {
      return '';
    } else if (this.dateBlocks.currentDateMonth.length && this.dateBlocks.currentDateYear.length === 0) {
      return this.dateBlocks.currentDateMonth;
    } else if (
      this.dateBlocks.currentDateMonth.length === 2 &&
      this.dateBlocks.currentDateYear.length === 1 &&
      this.dateBlocks.previousDateYear.length === 0
    ) {
      return this.dateBlocks.currentDateMonth + '/' + this.dateBlocks.currentDateYear;
    } else if (
      (this.dateBlocks.currentDateMonth.length === 2 &&
        this.dateBlocks.currentDateYear.length === 1 &&
        (this.dateBlocks.previousDateYear.length === 1 || this.dateBlocks.previousDateYear.length === 2)) ||
      (this.dateBlocks.currentDateMonth.length === 2 && this.dateBlocks.currentDateYear.length === 2)
    ) {
      return this.dateBlocks.currentDateMonth + '/' + this.dateBlocks.currentDateYear;
    }
  }

  private dateFixed(value: string): string {
    const date = ['', ''];
    const month = value.slice(0, 2);
    const year = value.slice(2, 4);
    return this.dateISO(date, [month, year]);
  }
}
