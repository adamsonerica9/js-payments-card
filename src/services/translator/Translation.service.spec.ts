/**
 * @jest-environment jsdom
 */

import cy_GB from '../../shared/translations/cy_GB.json';
import da_DK from '../../shared/translations/da_DK.json';
import de_DE from '../../shared/translations/de_DE.json';
import en_GB from '../../shared/translations/en_GB.json';
import en_US from '../../shared/translations/en_US.json';
import es_ES from '../../shared/translations/es_ES.json';
import fr_FR from '../../shared/translations/fr_FR.json';
import nl_NL from '../../shared/translations/nl_NL.json';
import no_NO from '../../shared/translations/no_NO.json';
import sv_SE from '../../shared/translations/sv_SE.json';
import { Translator } from './Translator.service';

describe('translate()', () => {

  it('should leave english unchanged', () => {
    const translator = new Translator('en_GB');
    expect(translator.translate('Expiration date')).toBe('Expiration date');
  });

  it('should translate to french', () => {
    const translator = new Translator('fr_FR');
    expect(translator.translate('Expiration date')).toBe('Date d\'expiration');
  });

  it('should translate to german', () => {
    const translator = new Translator('de_DE');
    expect(translator.translate('Expiration date')).toBe('Ablaufdatum');
  });

  it('should have translations for all Language parameters', () => {
    const translations = [en_GB, cy_GB, da_DK, de_DE, en_US, es_ES, fr_FR, nl_NL, no_NO, sv_SE];
    for (const i in translations) {
      const translation: Record<string, string> = translations[i];
      const language: Record<string, string> = Translator.translations;
      for (const key in language) {
        const text = language[key];
        expect(translation[text]).toBeDefined();
      }
    }
  });
});
