import cy_GB from '../../shared/translations/cy_GB.json';
import da_DK from '../../shared/translations/da_DK.json';
import de_DE from '../../shared/translations/de_DE.json';
import en_GB from '../../shared/translations/en_GB.json';
import en_US from '../../shared/translations/en_US.json';
import es_ES from '../../shared/translations/es_ES.json';
import fr_FR from '../../shared/translations/fr_FR.json';
import it_IT from '../../shared/translations/it_IT.json';
import nl_NL from '../../shared/translations/nl_NL.json';
import no_NO from '../../shared/translations/no_NO.json';
import sv_SE from '../../shared/translations/sv_SE.json';

export class Translator {
  static get translations(): {
    LABEL_CARD_NUMBER: string;
    LABEL_EXPIRATION_DATE: string;
    LABEL_SECURITY_CODE: string;
    PLACEHOLDER_EXPIRATION_DATE: string;
  } {
    return {
      LABEL_CARD_NUMBER: 'Card number',
      LABEL_EXPIRATION_DATE: 'Expiration date',
      LABEL_SECURITY_CODE: 'Security code',
      PLACEHOLDER_EXPIRATION_DATE: 'MM/YY',
    };
  }

  // The reference to grab our translations from
  private translationLookup: object;

  constructor(locale: string) {
    // All the languages we support
    const all = {
      cy_GB: cy_GB,
      da_DK: da_DK,
      de_DE: de_DE,
      en_GB: en_GB,
      en_US: en_US,
      es_ES: es_ES,
      fr_FR: fr_FR,
      it_IT: it_IT,
      nl_NL: nl_NL,
      no_NO: no_NO,
      sv_SE: sv_SE,
    };

    //
    this.translationLookup = all[locale ?? 'en_GB'];
  }

  translate = (text: string): string => this.translationLookup[text] ?? text;
}
