screenshots = {
    'scrn_animated_card_AMEX': 'animatedCardAMEX.png',
    'scrn_animated_card_VISA': 'animatedCardVISA.png',
    'scrn_animated_card_ASTROPAYCARD': 'animatedCardASTROPAYCARD.png',
    'scrn_animated_card_DINERS': 'animatedCardDINERS.png',
    'scrn_animated_card_DISCOVER': 'animatedCardDISCOVER.png',
    'scrn_animated_card_JCB': 'animatedCardJCB.png',
    'scrn_animated_card_MAESTRO': 'animatedCardMAESTRO.png',
    'scrn_animated_card_MASTERCARD': 'animatedCardMASTERCARD.png',
    'scrn_animated_card_card_back_side': 'animatedCardBackSide.png',
    'scrn_animated_card_blank_card': 'animatedCardBlank.png'
}
