""" This class consist all methods related with different waits
"""
import time

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


class Waits:

    def __init__(self, driver_factory, configuration):
        self._driver_factory = driver_factory
        self._driver = self._driver_factory.get_driver()
        self._timeout = configuration.TIMEOUT
        self._wait = WebDriverWait(self._driver, self._timeout)

    def wait_until_alert_is_presented(self):
        try:
            return self._wait.until(ec.alert_is_present())
        except TimeoutException:
            print(f'Alert was not presented in {self._timeout} seconds')

    def wait_until_iframe_is_presented_and_switch_to_it(self, iframe_name):
        # pylint: disable=bare-except
        max_try = 10
        while max_try:
            try:
                return self._wait.until(ec.frame_to_be_available_and_switch_to_it(iframe_name))
            except:
                print(f'Couldnt switch to iframe, will try {max_try} times more')
            time.sleep(0.2)
            max_try -= 1
        raise Exception('Iframe was unavailable within timeout')

    def wait_for_javascript(self):
        time.sleep(1)
        self._wait.until(lambda driver: self._driver.execute_script('return document.readyState') == 'complete')

    def wait_for_element_to_be_displayed(self, locator, max_try: int = 360):
        # pylint: disable=bare-except

        while max_try:
            try:
                is_element_displayed = self._driver.find_element(*locator).is_displayed()
                if is_element_displayed:
                    max_try = 0
                    return
                else:
                    max_try -= 1
            except:
                time.sleep(0.2)
                max_try -= 1
        raise Exception('Element not found within timeout')

    def switch_to_default_content(self):
        self._driver.switch_to.default_content()
