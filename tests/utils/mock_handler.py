from enum import Enum


class MockUrl(Enum):
    BASE_URI = "https://merchant.example.com:8443"
    WEBSERVICES_DOMAIN = "https://webservices.securetrading.net:8443"
    THIRDPARTY_URL = "https://library.securetrading.net:8443"
    VISA_MOCK_URI = "/visaPaymentStatus"
    CC_MOCK_ACS_URI = "/cardinalAuthenticateCard"
    APPLEPAY_MOCK_URI = "/applePaymentStatus"
    GATEWAY_MOCK_URI = "/jwt/"
    CONFIG_MOCK_URI = "/config.json"
    PORT = 8443
