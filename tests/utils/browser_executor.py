""" This class consist all methods related with browser activities"""


class BrowserExecutor:

    def __init__(self, driver_factory, waits):
        self._driver_factory = driver_factory
        self._driver = driver_factory.get_driver()
        self._waits = waits

    def open_page(self, page_url):
        self._driver.get(page_url)

    def close_browser(self):
        self._driver_factory.close_browser()

    def clear_cookies(self):
        self._driver.delete_all_cookies()

    def scroll_to_bottom(self):
        self._driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")

    def get_session_id(self):
        return self._driver.session_id
