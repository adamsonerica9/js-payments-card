""" This class consist all necessary web elements extensions methods
"""

from utils.waits import Waits


class Actions:

    def __init__(self, driver_factory, waits):
        self._driver = driver_factory.get_driver()
        self._waits = waits

    def send_keys(self, locator, string):
        self._waits.wait_for_element_to_be_displayed(locator)
        element = self.find_element(locator)
        element.send_keys(string)

    def send_key_one_by_one(self, locator, string):
        for x in string:
            self.send_keys(locator, x)

    def click(self, locator):
        self._waits.wait_for_element_to_be_displayed(locator)
        element = self.find_element(locator)
        element.click()

    def find_element(self, locator):
        # self.wait_for_ajax()
        element = self._driver.find_element(*locator)
        # * collects all the positional arguments in a tuple
        return element

    def is_element_displayed(self, locator):
        try:
            element = self._driver.find_element(*locator).is_displayed()
            return element is not None
        except:
            return False

    def find_elements(self, locator):
        # self.wait_for_ajax()
        elements = self._driver.find_elements(*locator)
        return elements

    def get_text(self, locator):
        element = self.find_element(locator)
        return element.text

    def get_element_attribute(self, locator, attribute_name):
        element = self.find_element(locator)
        return element.get_attribute(attribute_name)

    def is_element_enabled(self, locator):
        element = self.find_element(locator)
        is_enabled = element.is_enabled()
        return is_enabled

    def switch_to_iframe(self, iframe_name):
        self._waits.wait_until_iframe_is_presented_and_switch_to_it(iframe_name)
