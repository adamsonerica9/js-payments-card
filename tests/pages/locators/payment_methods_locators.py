from dataclasses import dataclass

from selenium.webdriver.common.by import By


@dataclass
class PaymentMethodsLocators:
    pay_mock_button: By = (By.ID, 'merchant-submit-button')
    not_private_connection_text: By = (By.XPATH, "//*[contains(text(),'This Connection Is Not Private')]")
