"""
PageFactory module is responsible for creation of a Page objects
"""
from enum import Enum

from pages.animated_card_page import AnimatedCardPage


class Pages(Enum):
    ANIMATED_CARD_PAGE = AnimatedCardPage


class PageFactory:
    def __init__(self, browser_executor, actions, reporter, configuration, waits):
        self._browser_executor = browser_executor
        self._actions = actions
        self._reporter = reporter
        self._config = configuration
        self._waits = waits

    def get_page(self, page_name):
        """Get page name method"""
        return page_name.value(browser_executor=self._browser_executor, actions=self._actions, reporter=self._reporter,
                               config=self._config, waits=self._waits)
