@visual_regression
Feature: Animated Card tests
  As a user
  I want to check animated card view

  Background:
    Given User opens page with animated card

  @scrn_animated_card
  Scenario Outline: <card_type> animated card view
    When User fills payment form with data: "<card_number>", "<expiration_date>"
    And User changes field focus to animated card
    Then Screenshot for <card_type> is taken after 2 seconds and checked

    @visual_regression_smoke
    Examples:
      | card_number      | expiration_date | card_type  |
      | 5100000000000511 | 12/29           | MASTERCARD |
      | 4111110000000211 | 12/29           | VISA       |

    Examples:
      | card_number         | expiration_date | card_type    |
      | 340000000000611     | 12/29           | AMEX         |
      | 1801000000000901    | 12/29           | ASTROPAYCARD |
      | 3000000000000111    | 12/29           | DINERS       |
      | 6011000000000301    | 12/29           | DISCOVER     |
      | 3528000000000411    | 12/29           | JCB          |
      | 5000000000000611    | 12/29           | MAESTRO      |

  @visual_regression_smoke @scrn_animated_card
  Scenario: Back side of animated card view
    When User fills payment form with data: "4111110000000211", "12/29" and "123"
    Then Screenshot for card_back_side is taken after 2 seconds and checked

  @visual_regression_smoke @scrn_animated_card
  Scenario: Blank animated card view
    Then Screenshot for blank_card is taken after 2 seconds and checked
