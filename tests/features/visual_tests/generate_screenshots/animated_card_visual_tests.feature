Feature: Animated Card tests
  As a user
  I want to check animated card view

  #feature just for screenshot creation purposes (e.g. when you need to create them again)

  Background:
    Given User opens page with animated card

  @visual_regression_generation @scrn_animated_card
  Scenario Outline: <card_type> animated card view
    When User fills payment form with data: "<card_number>", "<expiration_date>"
    Then Make screenshot after 2 seconds for <card_type>

    Examples:
      | card_number         | expiration_date | card_type    |
      | 340000000000611     | 12/29           | AMEX         |
      | 4111110000000211    | 12/29           | VISA         |
      | 1801000000000901    | 12/29           | ASTROPAYCARD |
      | 3000000000000111    | 12/29           | DINERS       |
      | 6011000000000301    | 12/29           | DISCOVER     |
      | 3528000000000411    | 12/29           | JCB          |
      | 5000000000000611    | 12/29           | MAESTRO      |
      | 5100000000000511    | 12/29           | MASTERCARD   |

  @visual_regression_generation @scrn_animated_card
  Scenario: Back side of animated card view
    When User fills payment form with data: "4111110000000211", "12/29" and "123"
    Then Make screenshot after 2 seconds for card_back_side

  @visual_regression_generation @scrn_animated_card
  Scenario: Blank animated card view
    Then Make screenshot after 2 seconds for blank_card
