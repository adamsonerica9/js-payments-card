@animated_card_repo_test
Feature: Animated Card tests
  As a user
  I want to check payment card functionality and
  input fields validation

  Background:
    Given User opens page with animated card

  Scenario Outline: Credit card recognition for <card_type> and validate date on animated card
    When User fills payment form with data: "<card_number>", "<exp_date>" and "<cvv>"
    Then User will see card icon connected to card type <card_type>
    And User will see the same provided data on animated credit card
    And User will see that animated card is flipped, except for "AMEX"

    @animated_card_repo_test_smoke
    Examples:
      | card_number         | exp_date | cvv | card_type  |
      | 4111 1100 0000 0211 | 12/22    | 123 | VISA       |
      | 5100 0000 0000 0511 | 12/23    | 123 | MASTERCARD |

    Examples:
      | card_number         | exp_date | cvv  | card_type    |
      | 3400 000000 00611   | 12/23    | 1234 | AMEX         |
      | 1801 0000 0000 0901 | 12/23    | 123  | ASTROPAYCARD |
      | 3000 000000 000111  | 12/23    | 123  | DINERS       |
      | 6011 0000 0000 0301 | 12/23    | 123  | DISCOVER     |
      | 3528 0000 0000 0411 | 12/23    | 123  | JCB          |
      | 5000 0000 0000 0611 | 12/23    | 123  | MAESTRO      |

  Scenario Outline: Checking animated card translation for <language>
    When User fills payment form with data: "340000000000611", "12/22" and "123"
    Then User will see that labels displayed on animated card are translated into "<language>"

    Examples:
      | language |
      | en_GB    |
#        | es_ES    |
#        | sv_SE    |

  Scenario Outline: Filling payment form with empty fields -> cardNumber "<card_number>", expiration: "<exp_date>", cvv: "<cvv>"
    When User fills payment form with invalid data: "<card_number>", "<exp_date>" and "<cvv>"
    And User changes the field focus
    Then User will see "Field is required" message under no-iframe-field: "<field>"
    And User will see that "<field>" no-iframe-field is highlighted

    Examples:
      | card_number      | exp_date | cvv  | field         |
      | None             | 12/22    | 123  | CARD_NUMBER   |
      | 4000000000001000 | None     | 123  | EXPIRATION_DATE      |
      | 4000000000001000 | 12/22    | None | SECURITY_CODE |

  Scenario: Filling 3-number of cvc code for AMEX card
    When User fills payment form with invalid data: "340000000000611", "12/22" and "123"
    And User changes the field focus
    Then User will see "Value mismatch pattern" message under no-iframe-field: "SECURITY_CODE"

  Scenario Outline: Filling payment form with incorrect pattern data -> cardNumber "<card_number>", expiration: "<expiration>", cvv: "<cvv>"
    When User fills payment form with invalid data: "<card_number>", "<expiration>" and "<cvv>"
    And User changes the field focus
    Then User will see "Value mismatch pattern" message under no-iframe-field: "<field>"
    And User will see that "<field>" no-iframe-field is highlighted

    @animated_card_repo_test_smoke
    Examples:
      | card_number      | expiration | cvv | field         |
      | 4000000000001000 | 12/22      | 12  | SECURITY_CODE |
      | 40000000         | 12/22      | 123 | CARD_NUMBER   |
      | 4000000000001000 | 12         | 123 | EXPIRATION_DATE      |

    Examples:
      | card_number      | expiration | cvv | field       |
      | 4000000000009999 | 12/22      | 123 | CARD_NUMBER |
      | 4000000000001000 | 44/22      | 123 | EXPIRATION_DATE    |
