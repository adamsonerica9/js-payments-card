"""This module consist Behave hooks which allows us to better manage the code workflow"""
# -*- coding: utf-8 -*-

# -- FILE: features/environment.py
# USE: behave -D BEHAVE_DEBUG_ON_ERROR         (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=yes     (to enable  debug-on-error)
# USE: behave -D BEHAVE_DEBUG_ON_ERROR=no      (to disable debug-on-error)

from logging import INFO

from configuration import CONFIGURATION
from utils.logger import get_logger
from pages.page_factory import PageFactory
from utils.browser_executor import BrowserExecutor

from utils.driver_factory import DriverFactory
from utils.actions import Actions
from utils.helpers.request_executor import mark_test_as_failed, set_scenario_name, mark_test_as_passed, \
    clear_shared_dict
from utils.reporter import Reporter

from utils.visual_regression.screenshot_manager import ScreenshotManager
from utils.waits import Waits

BEHAVE_DEBUG_ON_ERROR = False
LOGGER = get_logger(INFO)


def setup_debug_on_error(userdata):
    """Debug-on-Error(in case of step failures) providing, by using after_step() hook.
    The debugger starts when step definition fails"""
    global BEHAVE_DEBUG_ON_ERROR
    BEHAVE_DEBUG_ON_ERROR = userdata.getbool('BEHAVE_DEBUG_ON_ERROR')


def before_all(context):
    """Run before the whole shooting match"""
    context.configuration = CONFIGURATION


def before_scenario(context, scenario):
    """Run before each scenario"""
    LOGGER.info('BEFORE SCENARIO')
    clear_shared_dict()
    if context.configuration.REMOTE:
        context.configuration.BROWSER = context.configuration.REMOTE_BROWSER
    context.browser = context.configuration.BROWSER
    driver_factory = DriverFactory(configuration=context.configuration)
    context.waits = Waits(driver_factory=driver_factory, configuration=context.configuration)
    actions = Actions(driver_factory=driver_factory, waits=context.waits)
    context.browser_executor = BrowserExecutor(driver_factory=driver_factory, waits=context.waits)
    context.reporter = Reporter(driver=driver_factory, configuration=context.configuration)
    context.screenshot_manager = ScreenshotManager(driver=driver_factory, configuration=context.configuration)
    context.page_factory = PageFactory(browser_executor=context.browser_executor, actions=actions,
                                       reporter=context.reporter, configuration=context.configuration,
                                       waits=context.waits)
    context.session_id = context.browser_executor.get_session_id()
    scenario.name = '%s executed on %s' % (scenario.name, context.browser.upper())
    LOGGER.info(scenario.name)


def after_step(context, step):
    """Run after each step"""
    if step.status == 'failed':
        feature_name = _clean(context.feature.name.title())
        scenario_name = _clean(context.scenario.name.title())
        step_name = _clean(step.name.title())
        filename = f'{feature_name}_{scenario_name}_{step_name}'
        context.screenshot_manager.make_screenshot_for_visual_tests(filename, date_postfix=True)


def after_scenario(context, scenario):
    """Run after each scenario"""
    LOGGER.info('AFTER SCENARIO')
    browser_name = context.browser
    context.browser_executor.clear_cookies()
    context.browser_executor.close_browser()
    set_scenario_name(context.session_id, scenario.name)
    scenario.name = f'{scenario.name}_{browser_name.upper()}'
    if scenario.status == 'failed' and context.configuration.REMOTE:
        mark_test_as_failed(context.session_id)
    elif context.configuration.REMOTE:
        mark_test_as_passed(context.session_id)


def after_all(context):
    """Run after the whole shooting match"""
    # context.page_factory = PageFactory()
    #
    # executor = ioc_config.EXECUTOR.resolve('test')
    # executor.close_browser()
    pass


def _clean(text_to_clean):
    """Method to clean text which will be used for tests run reporting"""
    text = ''.join(x if x.isalnum() else '' for x in text_to_clean)
    return text
