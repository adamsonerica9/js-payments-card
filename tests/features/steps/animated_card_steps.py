import time

from assertpy import assert_that
from behave import *

from configuration import CONFIGURATION
from pages.page_factory import Pages
from utils.enums.screenshots import screenshots
from utils.enums.field_type import FieldType
from utils.helpers.request_executor import add_to_shared_dict
from utils.mock_handler import MockUrl

use_step_matcher("re")


@step("User opens page with animated card")
def step_impl(context):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.open_page(CONFIGURATION.URL.BASE_URL)
    if 'Safari' in context.browser:
        animated_card_page.open_page(MockUrl.WEBSERVICES_DOMAIN.value)
        animated_card_page.open_page(MockUrl.THIRDPARTY_URL.value)
        animated_card_page.open_page(CONFIGURATION.URL.BASE_URL)
        animated_card_page.open_page_with_not_private_connection_check(CONFIGURATION.URL.BASE_URL)

    context.waits.wait_for_javascript()


@when('User fills payment form with data: '
      '"(?P<card_number>.+)", '
      '"(?P<exp_date>[0-9]{2}/[0-9][0-9])"'
      '(?P<ignore> and )?"?(?P<cvv>[0-9]+)?"?')
def step_fill_payment(context, card_number, exp_date, ignore, cvv):
    context.pan = card_number
    context.exp_date = exp_date
    context.cvv = cvv
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.fill_payment_form_without_iframe(card_number, exp_date, cvv)


@when('User fills payment form with invalid data: "(?P<card_number>.+)", "(?P<exp_date>.+)" and "(?P<cvv>.+)"')
def step_impl(context, card_number, exp_date, cvv):
    step_fill_payment(context, card_number, exp_date, 'ignore', cvv)


@then("User will see card icon connected to card type (?P<card_type>.+)")
def step_impl(context, card_type):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.scroll_to_bottom()
    animated_card_page.validate_credit_card_icon(card_type)
    context.card_type = card_type


@step(
    'User will see the same provided data on animated credit card')
def step_impl(context):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.validate_all_data_on_animated_card(context.pan, context.exp_date, context.cvv, context.card_type)


@step('User will see that animated card is flipped, except for "AMEX"')
def step_impl(context):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.validate_if_animated_card_is_flipped(context.card_type)


@step("User changes the field focus")
def step_impl(context):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.change_field_focus()


@step('User will see that "(?P<field>.+)" no-iframe-field is highlighted')
def step_impl(context, field):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.validate_if_no_iframe_field_is_highlighted(FieldType[field].name)


@then('User will see "(?P<expected_message>.+)" message under no-iframe-field: "(?P<field>.+)"')
def step_impl(context, expected_message, field):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.validate_no_iframe_field_validation_message(FieldType[field].name, expected_message)


@then('User will see that labels displayed on animated card are translated into "(?P<language>.+)"')
def step_impl(context, language):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.scroll_to_bottom()
    animated_card_page.validate_animated_card_translation(language)


@then('User will see "(?P<field>.+)" field is disabled')
def step_impl(context, field):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.validate_field_accessibility(field, should_be_enabled=False)


@step('Make screenshot after (?P<how_many_seconds>.+) seconds for (?P<card_type>.+)')
def step_impl(context, how_many_seconds, card_type):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.scroll_to_bottom()
    time.sleep(int(how_many_seconds))
    screenshot_filename = _browser_device(context) + '_' + screenshots[
        _screenshot_tag(context.scenario.tags, card_type)]
    context.screenshot_manager.make_screenshot_for_visual_tests(screenshot_filename, date_postfix=True)


@then('Screenshot for (?P<card_type>.+) is taken after (?P<how_many_seconds>.+) seconds and checked')
def step_impl(context, card_type, how_many_seconds):
    # pylint: disable=invalid-name)
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.scroll_to_bottom()
    time.sleep(float(how_many_seconds))
    sm = context.screenshot_manager
    expected_screenshot_filename = _browser_device(context) + '_' + screenshots[
        _screenshot_tag(context.scenario.tags, card_type)]
    actual_screenshot_filename = sm.make_screenshot_for_visual_tests(expected_screenshot_filename, date_postfix=True)
    assertion_message = f'\nScreenshots comparator detected differences between ' \
                        f'"expected/{expected_screenshot_filename}" and ' \
                        f'"actual/{actual_screenshot_filename}"\n' \
                        f'Check the result file "results/{actual_screenshot_filename}"'
    add_to_shared_dict("assertion_message", assertion_message)
    if card_type in 'card_back_side':
        if sm.compare_screenshots(expected_screenshot_filename.replace('.png', '_ver1.png'),
                                  actual_screenshot_filename):
            return
        else:
            expected_screenshot_filename = expected_screenshot_filename.replace('.png', '_ver2.png')

    assert sm.compare_screenshots(expected_screenshot_filename, actual_screenshot_filename), assertion_message


def _screenshot_tag(tags, card_type):
    for tag in tags:
        if tag.startswith('scrn_'):
            return tag + '_' + card_type
    raise Exception('There is no screenshot tag!')


def _browser_device(context):
    name = CONFIGURATION.REMOTE_DEVICE
    if not name or name is None:
        name = context.browser
    name = name.upper()

    assert_that(name).is_in('CHROME', 'SAFARI', 'SAMSUNG GALAXY S21', 'IPHONE 13')

    return {
        'CHROME': name,
        'SAFARI': name,
        'SAMSUNG GALAXY S21': 'SGS21',
        'IPHONE 13': 'IP13',
    }[name]


@step('User changes field focus to animated card')
def step_impl(context):
    animated_card_page = context.page_factory.get_page(Pages.ANIMATED_CARD_PAGE)
    animated_card_page.click_on_animated_card()
